<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package smartmoments
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
			$searchResults = 'Search Results';
			$noResults = 'Sorry, no search results found.';
			if (function_exists('pll__')) {
				$searchResults = pll__('Search Results');
				$noResults = pll__('Sorry, no search results found.');
			}
		?>
			<div class="main-container">
				<h1 class="search-results-title"><?php echo esc_attr($searchResults); ?></h1>
				<div class="search-results-container">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="search-result-item-container">
						<div class="search-result-item">
							<a class="search-result-item-title" href="<?php echo get_permalink(); ?>">
								<h3><?php the_title(); ?></h3>
							</a>
							<?php if ( get_post_type() === 'post' ) : ?>
							<p class="search-result-item-desc">
								<?php the_field('article_description'); ?>
							</p>
							<?php else : ?>
							<p class="search-result-item-desc">
								<?php echo get_post_meta($post->ID, '_yoast_wpseo_metadesc', true); ?>
							</p>
							<?php endif; ?>
						</div>
					</div>
					
					<?php endwhile; ?>
				<?php else : ?>
					<p class="search-results-none"><?php echo esc_attr($noResults); ?></p>
				<?php endif; ?>
				</div>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
