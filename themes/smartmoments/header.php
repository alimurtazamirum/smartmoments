<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smartmoments
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5N7DX9');</script>
  <!-- End Google Tag Manager -->

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <!-- FORMASSEMBLY HEAD -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript">
      document.addEventListener("DOMContentLoaded", function(){
          const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
          let formElement = document.getElementById("tfa_0");
          if (null === formElement) {
              formElement = document.getElementById("0");
          }
          let appendJsTimerElement = function(){
              let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
              let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
              if (null !== cumulatedTimeElement) {
                  let cumulatedTime = parseInt(cumulatedTimeElement.value);
                  if (null !== cumulatedTime && cumulatedTime > 0) {
                      formTimeDiff += cumulatedTime;
                  }
              }
              let jsTimeInput = document.createElement("input");
              jsTimeInput.setAttribute("type", "hidden");
              jsTimeInput.setAttribute("value", formTimeDiff.toString());
              jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
              jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
              jsTimeInput.setAttribute("autocomplete", "off");
              if (null !== formElement) {
                  formElement.appendChild(jsTimeInput);
              }
          };
          if (null !== formElement) {
              if(formElement.addEventListener){
                  formElement.addEventListener('submit', appendJsTimerElement, false);
              } else if(formElement.attachEvent){
                  formElement.attachEvent('onsubmit', appendJsTimerElement);
              }
          }
      });
  </script>

  <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-layout.css?v=532-1" rel="stylesheet" type="text/css" />

  <link href="https://frieslandcampina.tfaforms.net/uploads/themes/theme-2.css" rel="stylesheet" type="text/css" />
  <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-jsonly.css?v=532-1" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
  <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/wforms.js?v=532-1"></script>
  <script type="text/javascript">
      wFORMS.behaviors.prefill.skip = false;
  </script>
  <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/localization-en_GB.js?v=532-1"></script>
  <!-- END FORMASSEMBLY HEAD -->
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5N7DX9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'smartmoments' ); ?></a>

  <header id="masthead" class="header">
    <div class="header-container">
      <div class="header-branding">
        <?php the_custom_logo(); ?>
      </div><!-- .site-branding -->
      <button class="header-mobile-btn">
        <span></span>
        <span></span>	
        <span></span>	
      </button>
      <div class="header-controls">
        <div class="header-menu">
          <div class="header-social-container">
            <?php if(get_field('facebook_url', 'option')) : ?>
            <a class="header-social-icon" href="<?php the_field('facebook_url', 'option'); ?>" target="_blank">
              <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon-fb.png" alt="Facebook">
            </a>
            <?php endif; ?>
            <?php if(get_field('youtube_url', 'option')) : ?>
            <a class="header-social-icon" href="<?php the_field('youtube_url', 'option'); ?>" target="_blank">
              <img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon-yt.png" alt="YouTube">
            </a>
            <?php endif; ?>
          </div>		
          <div class="header-lang-switch">
            <?php 
              if (function_exists('pll_the_languages')) {
                pll_the_languages(array('dropdown'=>1));
              }
            ?>
          </div>
          <div class="header-search">
            <?php get_search_form(); ?>
          </div>
        </div>
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'menu-1',
            'menu_id'         => 'primary-menu',
            'menu_class'      => 'header-nav-links',
            'container'       => 'nav',
            'container_class' => 'header-nav',
          ) );
        ?>
      </div>
    </div>
  </header><!-- #masthead -->
  <div id="important-overlay" class="important-overlay">
    <div class="important-notice">
      <?php
        $noticeTitle = 'Breast milk is best for your baby.';
        $noticeText = 'The World Health Organisation recommends exclusive breastfeeding for the first six months of life. Unnecessary introduction of bottle feeding or other food and drinks will have a negative impact on breastfeeding. After six months of age, infants should receive age appropriate foods while breastfeeding continues for up to two years of age or beyond. Consult your doctor before deciding to use infant formula or if you have difficulty breastfeeding.';
        $noticeLink1 = 'I have read and understood this';
        $noticeLink2 = 'No thanks, visit Friesland Campina\'s website.';
        if (function_exists('pll__')) {
          $noticeTitle = pll__('Important Notice Title');
          $noticeText = pll__('Important Notice Content');
          $noticeLink1 = pll__('I Agree');
          $noticeLink2 = pll__('I Disagree');
        }
      ?>
      <h1 class="important-notice-title"><?php echo esc_attr($noticeTitle); ?></h1>
      <p><?php echo esc_attr($noticeText); ?></p>
      <a id="important-accept-btn" class="imp-link" href="#"><?php echo esc_attr($noticeLink1); ?></a>
      <a class="imp-link alt" href="<?php the_field('friesland_url', 'option'); ?>"><?php echo esc_attr($noticeLink2); ?></a>
    </div>
  </div>
  
  <div id="content" class="site-content">
  
