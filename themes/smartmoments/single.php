<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package smartmoments
 */
get_header();
?>

	<?php
		$buyNow = 'Buy Now';
		$ctaTitle = 'Save time. Shop online!';
		$ctaDesc = 'Purchase Dutch Lady';
		$readMore = 'Read More';
		$sorry = 'Sorry, no articles found';
		$sidebarMore = 'You may also like';

		if (function_exists('pll__')) {
			$buyNow = pll__('Buy Now');
			$ctaTitle = pll__('Save time. Shop online!');
			$ctaDesc = pll__('Purchase Dutch Lady');
			$readMore = pll__('Read More');
			$sorry = pll__('Sorry, no articles found.');
			$sidebarMore = pll__('You may also like');
		}
	?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <div class="main-container">
        <div class="article-single-container">
          <div class="article-single-content-container">
          <?php while ( have_posts() ) : the_post(); ?>
            <article class="article-single-content">
              <h1 class="article-single-title"><?php the_title(); ?></h1>
              <?php the_post_thumbnail('large'); ?>
              <?php the_content(); ?>
            </article>
          <?php endwhile; ?>
            
          </div>
          <div class="article-single-sidebar-container">
            <article class="article-post-container">
              <div class="article-post">
                <div class="article-post-thumb-container">
                  <h3 class="article-post-title"><?php echo esc_attr($ctaTitle); ?></h3>
                  <img class="article-post-thumb" src="<?php bloginfo( 'template_url' ); ?>/assets/img/a_buy_now.jpg" alt="">
                </div>
                <p class="article-post-desc"><?php echo esc_attr($ctaDesc); ?></p>
                <a class="sm-btn-link" href="<?php the_field('buy_now_url', 'option'); ?>"><?php echo esc_attr($buyNow); ?></a>
              </div>
            </article>
						<h2 class="article-single-readmore"><?php echo esc_attr($sidebarMore); ?></h2>
						<?php 
							$categories = get_the_category($post->ID);
							$args = array(
								'posts_per_page'   => 3,
								'orderby'          => 'rand',
								'category_name'    => $categories[0]->slug,
								'post__not_in'     => array($post->ID),
							);
							$the_query = new WP_Query($args);
						?>
						<?php if  ($the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<article class="article-post-container">
								<div class="article-post">
									<div class="article-post-thumb-container">
										<h3 class="article-post-title"><?php the_title(); ?></h3>
										<?php the_post_thumbnail('medium', array('class' => 'article-post-thumb')); ?>
									</div>
									<p class="article-post-desc"><?php the_field('article_description'); ?></p>
									<a class="sm-btn-link" href="<?php echo get_permalink(); ?>"><?php echo $readMore; ?></a>
								</div>
							</article>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
						<?php else : ?>
						<p class="articles-no-results"><?php echo esc_attr($sorry); ?></p>
						<?php endif; ?>
          </div>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

