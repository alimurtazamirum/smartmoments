<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smartmoments
 */

?>

	</div><!-- #content -->
	<?php
		$taglineStr = 'The Smarter Choice';
		$disclaimerStr = '*Compared to previous formulated milk powder for children formulation (Year 2009)';
		$sitesStr = 'Our Other Sites';
		$copyrightStr = 'All rights reserved.';

		if (function_exists('pll__')) {
			$taglineStr = pll__('The Smarter Choice');
			$disclaimerStr = pll__('5x DHA Disclaimer');
			$sitesStr = pll__('Our Other Sites');
			$copyrightStr = pll__('Copyright');
		}
	?>

	<footer id="colophon" class="footer">
		<div class="footer-feature-container">
			<div class="footer-feature">
				<div class="footer-dha">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/footer_dha.jpg" alt="5x DHA">
					<h2 class="footer-dha-title"><?php echo esc_attr($taglineStr); ?></h2>
				</div>
				<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/footer_dl_products.jpg" alt="Dutch Lady Products">
			</div>
			<p class="footer-disclaimer"><?php echo esc_attr($disclaimerStr); ?></p>
		</div>
		<div class="footer-info">
			<h3 class="footer-title">
				<?php echo esc_attr($sitesStr); ?>
			</h3>
			<div class="footer-sites">
				<a class="footer-site-link" href="<?php the_field('friesland_url', 'option'); ?>" target="_blank">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/footer_fc.png" alt="FrieslandCampina">
				</a>
				<a class="footer-site-link" href="https://www.dutchlady.com.my/" target="_blank">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/footer_dl.png" alt="Dutch Lady">
				</a>
			</div>
			<div class="footer-bottom">
				<p class="footer-copyright"><span>©</span><?php echo date('Y'); ?> Dutch Lady Milk Industries Berhad (5063-V). <?php echo esc_attr($copyrightStr); ?></p>
				<?php
					wp_nav_menu( array(
						'theme_location'  => 'menu-2',
						'menu_id'         => 'secondary-menu',
						'menu_class'      => 'footer-nav-links',
						'container'       => 'nav',
						'container_class' => 'footer-nav',
					) );
				?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- <script type="text/javascript" id="cookieinfo"
		src="//cookieinfoscript.com/js/cookieinfo.min.js"
		data-bg="#009BF7"
		data-fg="#FFFFFF"
		data-link="#F1D600"
		data-moreinfo="https://www.smartmoments.com.my/en/privacy-policy/"
		data-cookie="CookieInfoScript"
		data-message="We use cookies to personalize and enhance your experience on our site. By clicking accept, you agree to our use of cookies. Visit our Privacy Policy to learn more."
		data-text-align="left"
       	data-close-text="Accept">
</script> -->
</body>
</html>
