<?php
/*
Template Name: Products Main Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $learnMore = 'Find Out More';
      if (function_exists('pll__')) {
        $learnMore = pll__('Find Out More');
      }
    ?>
      <?php
        $headerImg = get_field('header_image');
        $headerImgMob = get_field('header_image_mobile');
        if( !empty($headerImg) && !empty($headerImgMob) ):
      ?>
      <div class="sm-header-responsive" style="background-image: url('<?php echo $headerImg['url']; ?>');" >
      </div>
      <div class="sm-header-responsive mobile" style="background-image: url('<?php echo $headerImgMob['url']; ?>');" >
      </div>
      <?php endif; ?>
      <div class="main-container">
        <section class="products-main-info">
        <?php while ( have_posts() ) : the_post(); ?>
          <h1 class="products-main-info-title"><?php the_field('products_sub_headline'); ?></h1>
          <?php the_content(); ?>
        <?php endwhile; ?>
        </section>
        <?php
          $args = array(
            'posts_per_page'   => -1,
            'post_type'        => 'page',
            'order'            => 'ASC',
            'orderby'          => 'title',
            'post_parent'      => 15,
          );
          $the_query = new WP_Query($args);
        ?>
        <?php if  ($the_query->have_posts() ) : ?>
        <section class="products-range">
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <div class="products-range-item-container">
            <div class="products-range-item">
              <?php 
                $productImg = get_field('product_key_image');
                if( !empty($productImg) ):
              ?>
                <img src="<?php echo $productImg['url']; ?>" alt="<?php echo $productImg['alt']; ?>">
              <?php endif; ?>
              <h4 class="products-range-item-title"><?php the_field('product_name_&_age_range'); ?></h4>
              <a class="sm-btn-link" href="<?php the_permalink(); ?>"><?php echo esc_attr($learnMore); ?></a>
            </div>
          </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <?php endif; ?>
        <?php 
          $nutriImage = get_field('nutrition_info');
          if(!empty($nutriImage)):
        ?>
        <div class="products-nutrient-info">
          <img src="<?php echo $nutriImage['url']; ?>" alt="<?php echo $nutriImage['alt']; ?>">
        </div>
        <?php endif; ?>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

