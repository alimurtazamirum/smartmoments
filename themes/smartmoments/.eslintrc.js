module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    jquery: true,
  },
  extends: ["eslint:recommended", "wordpress"],
  parserOptions: {
    sourceType: "module"
  },
  rules: {
    indent: ["error", 2],
    quotes: ["error", "single"],
    semi: ["error", "always"],
    "linebreak-style": "off",
    "space-in-parens": "off",
    "vars-on-top": "off",
    "no-trailing-spaces": "off",
  }
};
