<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package smartmoments
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
      $notExist = 'The page you are looking for doesn\'t exist.';
      $goHome = 'Back to Home Page';

      if (function_exists('pll__')) {
				$notExist = pll__('The page you are looking for doesn\'t exist.');
				$goHome = pll__('Back to Home Page');
      }
    ?>
			<div class="main-container">
				<div class="not-found-container">
					<h1 class="not-found-title">Oops!</h1>
					<p class="not-found-desc"><?php echo esc_attr($notExist); ?></p>
					<a class="sm-btn-link lg" href="<?php echo get_site_url(); ?>"><?php echo esc_attr($goHome); ?></a>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
