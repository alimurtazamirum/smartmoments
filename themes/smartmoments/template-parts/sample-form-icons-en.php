<div id="form-overlay" class="important-overlay">
  <div id="modal-1" class="form-icon-modal">
    <h3 class="form-icon-title">Highest DHA</h3>
    <p>Did you know that by the age of 5, a child's brain is already 90% developed? One of the most important nutrients to support this process is Docosahexaenoic Acid (DHA); and in Dutch Lady 123, we have the HIGHEST^ DHA! Try it today.</p>
  </div>
  <div id="modal-2" class="form-icon-modal">
    <h3 class="form-icon-title">Great Flavours!</h3>
    <p>With multiple delicious flavours like Plain, Honey and Chocolate to choose from, your child will get to enjoy tasty, flavourful, and nutritious milk to keep their taste buds happy, as well as optimizing their growth.<br/>
    Try it today!</p>
  </div>
  <div id="modal-3" class="form-icon-modal">
    <h3 class="form-icon-title">More Nutrients</h3>
    <p>In the early years of life, a child needs sufficient nutrients for growth and development. They need protein for production of new cells, Vitamin B12 to produce red blood cells, and a necessary amount of DHA to support their brain development! Dutch Lady Formulated Milk Powder has +44.7%* protein, +125%* Vitamin B12, and also the HIGHEST DHA^ in Dutch Lady 123</p>
  </div>
  <div id="modal-4" class="form-icon-modal">
    <h3 class="form-icon-title">Our Heritage</h3>
    <p>First founded by Dutch farmer families in 1872, our rich heritage means we make our milk based on farming practices and knowledge that has been passed down through generations. From selecting the finest cows, to having the best technology, we're committed to producing great milk!</p>
  </div>
</div>

<div class="form-icons-container">
  <div class="form-icons-row">
    <div class="form-icon">
      <img id="1" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_DHA.png" alt="Highest^ DHA!">
    </div>
    <div class="form-icon">
      <img id="2" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Flavours.png" alt="Great Flavours">
    </div>
  </div>
  <div class="form-icons-row">
    <div class="form-icon">
      <img id="3" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Nutrients.png" alt="More Nutrients">
    </div>
    <div class="form-icon">
      <img id="4" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Heritage.png" alt="Our Heritage">
    </div>
  </div>
  <p class="form-icons-msg">(Click on the icons to learn more!)</p>
</div>