<div id="form-overlay" class="important-overlay">
  <div id="modal-1" class="form-icon-modal">
    <h3 class="form-icon-title">DHA Tertinggi</h3>
    <p>Tahukah anda, bahawa 90% dari perkembangan otak kanak-kanak berlaku sebelum mereka berumur 5 tahun? Antara nutrien yang penting dalam proses perkembangan otak, adalah Docosahexaenoic Acid ataupun DHA! Dan Dutch Lady 123 mengandungi DHA yang tertinggi^.</p>
  </div>
  <div id="modal-2" class="form-icon-modal">
    <h3 class="form-icon-title">Perisa Yang Lazat</h3>
    <p>Dengan adanya pilihan pelbagai perisa seperti Biasa, Madu, dan Coklat, anak anda dapat menikmati susu yang lazat – di samping bernutrisi, untuk terus membantu proses perkembangan dan tumbesaran mereka.</p>
  </div>
  <div id="modal-3" class="form-icon-modal">
    <h3 class="form-icon-title">Lebih Nutrien</h3>
    <p>Kanak-kanak memerlukan nutrisi yang seimbang dan mencukupi pada peringkat awal kehidupan untuk membantu dalam proses perkembangan otak dan tumbesaran badan. Protein adalah amat penting untuk memperbaiki sel-sel otot dan badan, sepertimana Vitamin B12 adalah penting untuk menghasilkan sel darah merah, dan DHA adalah penting untuk membantu perkembangan otak. Susu Tepung Rumusan untuk Kanak-kanak Dutch Lady mengandungi +44.7% protein, +125%</p>
  </div>
  <div id="modal-4" class="form-icon-modal">
    <h3 class="form-icon-title">Warisan Kami</h3>
    <p>Dutch Lady diasaskan oleh keluarga-keluarga peladang di Belanda pada 1872. Ini bermakna susu kami diperbuat berasaskan amalan peladang dan penternak, dan juga menggunakan ilmu yang telah diturunkan dari generasi ke generasi. Kami hanya memilih lembu yang terbaik, dan hanya teknologi yang tercanggih, supaya dapat mengeluarkan susu yang terbaik!</p>
  </div>
</div>

<div class="form-icons-container">
  <div class="form-icons-row">
    <div class="form-icon">
      <img id="1" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_DHA_BM.png" alt="DHA Tertinggi^!">
    </div>
    <div class="form-icon">
      <img id="2" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Flavours_BM.png" alt="Perisa Lazat">
    </div>
  </div>
  <div class="form-icons-row">
    <div class="form-icon">
      <img id="3" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Nutrients_BM.png" alt="Lebih Nutrien">
    </div>
    <div class="form-icon">
      <img id="4" src="<?php bloginfo( 'template_url' ); ?>/assets/img/Icon_Heritage_BM.png" alt="Warisan Kami">
    </div>
  </div>
  <p class="form-icons-msg">(Klik ikon untuk ketahui lebih lanjut!)</p>
</div>