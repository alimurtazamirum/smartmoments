<!-- FORM: HEAD SECTION -->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
            let formElement = document.getElementById("tfa_0");
            if (null === formElement) {
                formElement = document.getElementById("0");
            }
            let appendJsTimerElement = function(){
                let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
                let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
                if (null !== cumulatedTimeElement) {
                    let cumulatedTime = parseInt(cumulatedTimeElement.value);
                    if (null !== cumulatedTime && cumulatedTime > 0) {
                        formTimeDiff += cumulatedTime;
                    }
                }
                let jsTimeInput = document.createElement("input");
                jsTimeInput.setAttribute("type", "hidden");
                jsTimeInput.setAttribute("value", formTimeDiff.toString());
                jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("autocomplete", "off");
                if (null !== formElement) {
                    formElement.appendChild(jsTimeInput);
                }
            };
            if (null !== formElement) {
                if(formElement.addEventListener){
                    formElement.addEventListener('submit', appendJsTimerElement, false);
                } else if(formElement.attachEvent){
                    formElement.attachEvent('onsubmit', appendJsTimerElement);
                }
            }
        });
    </script>

    <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-layout.css?v=547-4" rel="stylesheet" type="text/css" />

    <link href="https://frieslandcampina.tfaforms.net/uploads/themes/theme-2.css" rel="stylesheet" type="text/css" />
    <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-jsonly.css?v=547-4" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/wforms.js?v=547-4"></script>
    <script type="text/javascript">
        wFORMS.behaviors.prefill.skip = false;
    </script>
        <link rel="stylesheet" type="text/css" href="https://frieslandcampina.tfaforms.net/css/kalendae.css" />
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/js/kalendae/kalendae.standalone.min.js" ></script>
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/wforms_calendar.js"></script>
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/localization-en_GB.js?v=547-4"></script>

    <!-- Script to get links from local storage -->
    <script type="text/javascript">
    window.addEventListener('load', function() {
        // set item to local storage 
        setitemsToSessionStorage();

        document.getElementById("tfa_140").value = sessionStorage.getItem('utm_source');
        document.getElementById("tfa_141").value = sessionStorage.getItem('utm_medium');
        document.getElementById("tfa_142").value = sessionStorage.getItem('utm_campaign');
        document.getElementById("tfa_144").value = sessionStorage.getItem('utm_content');
        
        document.getElementById("tfa_149").value = sessionStorage.getItem('utm_source');
        document.getElementById("tfa_150").value = sessionStorage.getItem('utm_medium');
        document.getElementById("tfa_151").value = sessionStorage.getItem('utm_campaign');
        document.getElementById("tfa_153").value = sessionStorage.getItem('utm_content');
    };

    function setitemsToSessionStorage() {
            var url = window.location.href,
            keyToFind1 = "utm_source",
            keyToFind2 = "utm_medium",
            keyToFind3 = "utm_campaign",
            keyToFind4 = "utm_content",
            keyToFind5 = "utm_term";
        if (-1 < url.indexOf(keyToFind1 + "\x3d")) {
            var valueFound = url.substr(url.indexOf(keyToFind1 + "\x3d") + keyToFind1.length + 1).split("\x26")[0];
            sessionStorage.setItem(keyToFind1, valueFound)
        } - 1 < url.indexOf(keyToFind2 + "\x3d") && (valueFound = url.substr(url.indexOf(keyToFind2 + "\x3d") + keyToFind2.length + 1).split("\x26")[0], sessionStorage.setItem(keyToFind2, valueFound)); - 1 < url.indexOf(keyToFind3 + "\x3d") && (valueFound = url.substr(url.indexOf(keyToFind3 + "\x3d") + keyToFind3.length + 1).split("\x26")[0], sessionStorage.setItem(keyToFind3, valueFound)); - 1 < url.indexOf(keyToFind4 + "\x3d") && (valueFound = url.substr(url.indexOf(keyToFind4 + "\x3d") + keyToFind4.length + 1).split("\x26")[0], sessionStorage.setItem(keyToFind4, valueFound)); - 1 < url.indexOf(keyToFind5 + "\x3d") && (valueFound = url.substr(url.indexOf(keyToFind5 + "\x3d") + keyToFind5.length + 1).split("\x26")[0], sessionStorage.setItem(keyToFind5, valueFound));
    };

 </script>    
    <!-- LOCAL STORAGE SCRIPT END-->

<!-- FORM: BODY SECTION -->
<div class="wFormContainer"  >
    <div class="wFormHeader"></div>
    <style type="text/css">
                #tfa_4,
                *[id^="tfa_4["] {
                    width: 277px !important;
                }
                #tfa_4-D,
                *[id^="tfa_4["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_159,
                *[id^="tfa_159["] {
                    width: 277px !important;
                }
                #tfa_159-D,
                *[id^="tfa_159["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_189,
                *[id^="tfa_189["] {
                    width: 277px !important;
                }
                #tfa_189-D,
                *[id^="tfa_189["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_8,
                *[id^="tfa_8["] {
                    width: 542px !important;
                }
                #tfa_8-D,
                *[id^="tfa_8["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_9,
                *[id^="tfa_9["] {
                    width: 234px !important;
                }
                #tfa_9-D,
                *[id^="tfa_9["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_20,
                *[id^="tfa_20["] {
                    width: 234px !important;
                }
                #tfa_20-D,
                *[id^="tfa_20["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_22,
                *[id^="tfa_22["] {
                    width: 322px !important;
                }
                #tfa_22-D,
                *[id^="tfa_22["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_24,
                *[id^="tfa_24["] {
                    width: 514px !important;
                }
                #tfa_24-D,
                *[id^="tfa_24["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_26,
                *[id^="tfa_26["] {
                    width: 514px !important;
                }
                #tfa_26-D,
                *[id^="tfa_26["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_27,
                *[id^="tfa_27["] {
                    width: 234px !important;
                }
                #tfa_27-D,
                *[id^="tfa_27["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_28,
                *[id^="tfa_28["] {
                    width: 240px !important;
                }
                #tfa_28-D,
                *[id^="tfa_28["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_29,
                *[id^="tfa_29["] {
                    width: 242px !important;
                }
                #tfa_29-D,
                *[id^="tfa_29["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_111,
                *[id^="tfa_111["] {
                    width: 274px !important;
                }
                #tfa_111-D,
                *[id^="tfa_111["][class~="field-container-D"] {
                    width: auto !important;
                }
            </style><div class=""><div class="wForm" id="46-WRPR" dir="ltr">
<div class="codesection" id="code-46"><style type="text/css">

#tfa_133-L {
color: red;
font-size: 110%;
}
  
#tfa_133 {
border-bottom-width: 3px;
border-top-width: 3px;
border-right-width: 3px;
border-left-width: 3px;
}  
 
#tfa_135-L {
color: black;
font-size: 110%;
}  
  
#tfa_136 {
color: black;  
font-size: 100%;
 
}
  
.entry-content a {
  color: blue;
}

</style></div>
<form method="post" action="https://frieslandcampina.tfaforms.net/responses/processor" class="hintsBelow labelsAbove" id="46" role="form">
<fieldset id="tfa_137" class="section wf-acl-hidden">
<legend id="tfa_137-L">Hidden Fields</legend>
<div id="tfa_145" class="section inline group">
<div class="oneField field-container-D    " id="tfa_138-D">
<label id="tfa_138-L" class="label preField " for="tfa_138">Last Campaign</label><br><div class="inputWrapper"><input type="text" id="tfa_138" name="tfa_138" value="Dutch Lady Mom's Club" default="Dutch Lady Mom's Club" title="Last Campaign" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="oneField field-container-D    " id="tfa_139-D">
<label id="tfa_139-L" class="label preField " for="tfa_139">Lead Source</label><br><div class="inputWrapper"><input type="text" id="tfa_139" name="tfa_139" value="Website" default="Website" title="Lead Source" data-dataset-allow-free-responses="" class=""></div>
</div>
</div>
<div id="tfa_146" class="section inline group">
<div class="oneField field-container-D    " id="tfa_140-D">
<label id="tfa_140-L" class="label preField " for="tfa_140">UTM_Source</label><br><div class="inputWrapper"><input type="text" id="tfa_140" name="tfa_140" value="" title="UTM_Source" data-dataset-allow-free-responses="" class="calc-UTMsource"></div>
</div>
<div class="oneField field-container-D    " id="tfa_141-D">
<label id="tfa_141-L" class="label preField " for="tfa_141">UTM_Medium</label><br><div class="inputWrapper"><input type="text" id="tfa_141" name="tfa_141" value="" title="UTM_Medium" data-dataset-allow-free-responses="" class="calc-UTMmedium"></div>
</div>
</div>
<div id="tfa_147" class="section inline group">
<div class="oneField field-container-D    " id="tfa_142-D">
<label id="tfa_142-L" class="label preField " for="tfa_142">UTM_Campaign</label><br><div class="inputWrapper"><input type="text" id="tfa_142" name="tfa_142" value="" title="UTM_Campaign" data-dataset-allow-free-responses="" class="calc-UTMcampaign"></div>
</div>
<div class="oneField field-container-D    " id="tfa_144-D">
<label id="tfa_144-L" class="label preField " for="tfa_144">UTM_Content</label><br><div class="inputWrapper"><input type="text" id="tfa_144" name="tfa_144" value="" title="UTM_Content" data-dataset-allow-free-responses="" class="calc-UTMcontent"></div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_197-D">
<label id="tfa_197-L" class="label preField " for="tfa_197">UTM (Concatenated)</label><br><div class="inputWrapper"><input type="text" id="tfa_197" name="tfa_197" value="" readonly title="UTM (Concatenated)" data-dataset-allow-free-responses="" class='formula=utmsource+"/"+utmmedium+"/"+utmcampaign+"/"+utmcontent readonly'></div>
</div>
<div class="oneField field-container-D    " id="tfa_151-D" role="group" aria-labelledby="tfa_151-L">
<label id="tfa_151-L" class="label preField "><b>ACTIVITY TYPE</b></label><br><div class="inputWrapper"><span id="tfa_151" class="choices vertical "><span class="oneChoice"><input type="checkbox" value="tfa_152" class="" checked data-default-value="true" id="tfa_152" name="tfa_152" aria-labelledby="tfa_152-L"><label class="label postField" id="tfa_152-L" for="tfa_152"><span class="input-checkbox-faux"></span>Sample Request</label></span></span></div>
</div>
</fieldset>
<div class="oneField field-container-D    " id="tfa_4-D">
<label id="tfa_4-L" class="label preField reqMark" for="tfa_4"><b>PLEASE SELECT THE SAMPLE PRODUCT YOU WOULD LIKE TO RECEIVE</b></label><br><div class="inputWrapper"><select id="tfa_4" name="tfa_4" title="PLEASE SELECT THE SAMPLE PRODUCT YOU WOULD LIKE TO RECEIVE" aria-required="true" class="calc-product required"><option value="">Please select...</option>
<option value="tfa_6" id="tfa_6" data-conditionals="#tfa_52,#tfa_159" class="calcval-DutchLady123">Dutch Lady 123</option>
<option value="tfa_163" id="tfa_163" data-conditionals="#tfa_52,#tfa_189" class="calcval-Dutch Lady 456">Dutch Lady 456</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_159-D">
<label id="tfa_159-L" class="label preField reqMark" for="tfa_159"><b>PLEASE SELECT A FLAVOR</b></label><br><div class="inputWrapper"><select id="tfa_159" name="tfa_159" data-condition="`#tfa_6`" title="PLEASE SELECT A FLAVOR" aria-required="true" class="calc-dlflavora required"><option value="">Please select...</option>
<option value="tfa_160" id="tfa_160" class="calcval-01t6F00000B6LlTQAV">Plain</option>
<option value="tfa_161" id="tfa_161" class="calcval-01t6F00000B6LlUQAV">Honey</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_189-D">
<label id="tfa_189-L" class="label preField reqMark" for="tfa_189"><b>PLEASE SELECT A FLAVOR</b></label><br><div class="inputWrapper"><select id="tfa_189" name="tfa_189" data-condition="`#tfa_163`" title="PLEASE SELECT A FLAVOR" aria-required="true" class="calc-dlflavorb required"><option value="">Please select...</option>
<option value="tfa_194" id="tfa_194" class="calcval-01t6F00000B6LlVQAV">Plain</option>
<option value="tfa_195" id="tfa_195" class="calcval-01t6F00000B6LlWQAV">Chocolate</option></select></div>
</div>
<fieldset id="tfa_181" class="section wf-acl-hidden">
<legend id="tfa_181-L">Product Selected</legend>
<div class="oneField field-container-D    " id="tfa_196-D">
<label id="tfa_196-L" class="label preField " for="tfa_196">Product chosen (ID)</label><br><div class="inputWrapper"><input type="text" id="tfa_196" name="tfa_196" value="" readonly title="Product chosen (ID)" data-dataset-allow-free-responses="" class='formula=if(product=="DutchLady123"){dlflavora}else{dlflavorb} readonly'></div>
</div>
</fieldset>
<fieldset id="tfa_7" class="section">
<legend id="tfa_7-L">Part 1: Basic Information</legend>
<div class="oneField field-container-D    " id="tfa_8-D">
<label id="tfa_8-L" class="label preField reqMark" for="tfa_8"><b>NAME (AS PER MYKAD/PASSPORT)</b></label><br><div class="inputWrapper"><input type="text" id="tfa_8" name="tfa_8" value="" aria-required="true" title="NAME (AS PER MYKAD/PASSPORT)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div id="tfa_21" class="section inline group">
<div class="oneField field-container-D    " id="tfa_9-D">
<label id="tfa_9-L" class="label preField reqMark" for="tfa_9"><b>MOBILE</b></label><br><div class="inputWrapper"><select id="tfa_9" name="tfa_9" title="MOBILE" aria-required="true" class="required"><option value="">Please select...</option>
<option value="tfa_10" id="tfa_10" class="">010</option>
<option value="tfa_11" id="tfa_11" class="">011</option>
<option value="tfa_12" id="tfa_12" class="">012</option>
<option value="tfa_13" id="tfa_13" class="">013</option>
<option value="tfa_14" id="tfa_14" class="">014</option>
<option value="tfa_15" id="tfa_15" class="">015</option>
<option value="tfa_16" id="tfa_16" class="">016</option>
<option value="tfa_17" id="tfa_17" class="">017</option>
<option value="tfa_18" id="tfa_18" class="">018</option>
<option value="tfa_19" id="tfa_19" class="">019</option></select></div>
</div>
<div class="oneField field-container-D  labelsHidden  " id="tfa_20-D">
<label id="tfa_20-L" class="label preField reqMark" for="tfa_20">NUMBER</label><br><div class="inputWrapper"><input type="text" id="tfa_20" name="tfa_20" value="" aria-required="true" autoformat="########" title="NUMBER" data-dataset-allow-free-responses="" class="validate-custom /^([0-9]{7,8})$/ required"></div>
<script type="text/javascript">
                        if(typeof wFORMS != 'undefined') {
                            if(wFORMS.behaviors.validation) {
                                wFORMS.behaviors.validation.rules['customtfa_20'] =  { selector: '*[id="tfa_20"]', check: 'validateCustom'};
                                wFORMS.behaviors.validation.messages['customtfa_20'] = "Please enter your 7 or 8-digit number.";
                            }
                        }</script>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_22-D">
<label id="tfa_22-L" class="label preField reqMark" for="tfa_22"><b>EMAIL ADDRESS</b></label><br><div class="inputWrapper"><input type="text" id="tfa_22" name="tfa_22" value="" aria-required="true" title="EMAIL ADDRESS" data-dataset-allow-free-responses="" class="validate-email required"></div>
</div>
</fieldset>
<fieldset id="tfa_23" class="section">
<legend id="tfa_23-L">Part 2: Where should we deliver your sample?</legend>
<div class="oneField field-container-D    " id="tfa_24-D">
<label id="tfa_24-L" class="label preField reqMark" for="tfa_24"><b>ADDRESS LINE 1&nbsp;<i>(House/Unit No., Condo/Apartment Name)</i></b></label><br><div class="inputWrapper"><input type="text" id="tfa_24" name="tfa_24" value="" aria-required="true" title="ADDRESS LINE 1 (House/Unit No., Condo/Apartment Name)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_26-D">
<label id="tfa_26-L" class="label preField reqMark" for="tfa_26"><b>ADDRESS LINE 2&nbsp;<i>(Street Name, District)</i></b></label><br><div class="inputWrapper"><input type="text" id="tfa_26" name="tfa_26" value="" aria-required="true" title="ADDRESS LINE 2 (Street Name, District)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div id="tfa_33" class="section inline group">
<div class="oneField field-container-D    " id="tfa_27-D">
<label id="tfa_27-L" class="label preField reqMark" for="tfa_27"><b>POST CODE</b></label><br><div class="inputWrapper"><input type="text" id="tfa_27" name="tfa_27" value="" aria-required="true" autoformat="#####" title="POST CODE" data-dataset-allow-free-responses="" class="validate-custom /^([0-9]{5})$/ required"></div>
<script type="text/javascript">
                        if(typeof wFORMS != 'undefined') {
                            if(wFORMS.behaviors.validation) {
                                wFORMS.behaviors.validation.rules['customtfa_27'] =  { selector: '*[id="tfa_27"]', check: 'validateCustom'};
                                wFORMS.behaviors.validation.messages['customtfa_27'] = "Please enter your 5-digit post code.";
                            }
                        }</script>
</div>
<div class="oneField field-container-D    " id="tfa_28-D">
<label id="tfa_28-L" class="label preField reqMark" for="tfa_28"><b>CITY</b></label><br><div class="inputWrapper"><input type="text" id="tfa_28" name="tfa_28" value="" aria-required="true" title="CITY" data-dataset-allow-free-responses="" class="required"></div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_29-D">
<label id="tfa_29-L" class="label preField reqMark" for="tfa_29"><b>STATE</b></label><br><div class="inputWrapper"><select id="tfa_29" name="tfa_29" title="STATE" aria-required="true" class="required"><option value="">Please select...</option>
<option value="tfa_30" id="tfa_30" class="">Johor Darul Takzim</option>
<option value="tfa_31" id="tfa_31" class="">Kedah Darul Aman</option>
<option value="tfa_32" id="tfa_32" class="">Kelantan Darul Naim</option>
<option value="tfa_34" id="tfa_34" class="">Melaka</option>
<option value="tfa_35" id="tfa_35" class="">Negeri Sembilan Darul Khusus</option>
<option value="tfa_36" id="tfa_36" class="">Pahang Darul Makmur</option>
<option value="tfa_37" id="tfa_37" class="">Perak Darul Ridzuan</option>
<option value="tfa_38" id="tfa_38" class="">Perlis Indera Kayangan</option>
<option value="tfa_39" id="tfa_39" class="">Pulau Pinang</option>
<option value="tfa_40" id="tfa_40" class="">Sabah</option>
<option value="tfa_41" id="tfa_41" class="">Sarawak</option>
<option value="tfa_42" id="tfa_42" class="">Selangor Darul Ehsan</option>
<option value="tfa_43" id="tfa_43" class="">Terengganu Darul Iman</option>
<option value="tfa_44" id="tfa_44" class="">Wilayah Persekutuan Kuala Lumpur</option>
<option value="tfa_45" id="tfa_45" class="">Wilayah Persekutuan Labuan</option>
<option value="tfa_46" id="tfa_46" class="">Wilayah Persekutuan Putrajaya</option></select></div>
</div>
</fieldset>
<fieldset id="tfa_52" class="section" data-condition="`#tfa_6` OR `#tfa_163`">
<legend id="tfa_52-L">Part 3: Tell us about your child</legend>
<div class="oneField field-container-D    " id="tfa_148-D">
<label id="tfa_148-L" class="label preField reqMark" for="tfa_148"><b>BIRTHDATE</b></label><br><div class="inputWrapper"><input type="text" id="tfa_148" name="tfa_148" value="" autocomplete="off" aria-required="true" min="2012-01-01" title="BIRTHDATE" data-dataset-allow-free-responses="" class="validate-datecal required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_111-D">
<label id="tfa_111-L" class="label preField " for="tfa_111"><b>BRAND OF MILK CONSUMED</b></label><br><div class="inputWrapper"><select id="tfa_111" name="tfa_111" title="BRAND OF MILK CONSUMED" class=""><option value="">Please select...</option>
<option value="tfa_112" id="tfa_112" class="">Abbott</option>
<option value="tfa_113" id="tfa_113" class="">Anmum Essential</option>
<option value="tfa_114" id="tfa_114" class="">Dumex</option>
<option value="tfa_115" id="tfa_115" class="">Dumex Mamil</option>
<option value="tfa_116" id="tfa_116" class="">Dutch Lady</option>
<option value="tfa_117" id="tfa_117" class="">Enfagrow</option>
<option value="tfa_118" id="tfa_118" class="">Fernleaf</option>
<option value="tfa_119" id="tfa_119" class="">Friso Gold</option>
<option value="tfa_120" id="tfa_120" class="">Gain Kid</option>
<option value="tfa_121" id="tfa_121" class="">Isomil</option>
<option value="tfa_122" id="tfa_122" class="">Lactogen</option>
<option value="tfa_123" id="tfa_123" class="">Morinaga</option>
<option value="tfa_124" id="tfa_124" class="">Nan</option>
<option value="tfa_125" id="tfa_125" class="">Novalac</option>
<option value="tfa_126" id="tfa_126" class="">Pediasure</option>
<option value="tfa_127" id="tfa_127" class="">S-26</option>
<option value="tfa_128" id="tfa_128" class="">Snow</option>
<option value="tfa_129" id="tfa_129" class="">Sustagen</option>
<option value="tfa_130" id="tfa_130" class="">Do not consume milk powder</option>
<option value="tfa_131" id="tfa_131" class="">Others</option></select></div>
</div>
</fieldset>
<fieldset id="tfa_133" class="section">
<legend id="tfa_133-L">Submit</legend>
<div id="tfa_150" class="section inline group">
<div class="oneField field-container-D  labelsRemoved  " id="tfa_134-D" role="group" aria-labelledby="tfa_134-L"><div class="inputWrapper"><span id="tfa_134" class="choices horizontal required"><span class="oneChoice"><input type="checkbox" value="tfa_135" class="" id="tfa_135" name="tfa_135" aria-labelledby="tfa_135-L"><label class="label postField" id="tfa_135-L" for="tfa_135"><span class="input-checkbox-faux"></span><span style="font-size: 14.4px;"><b>Agree to terms and conditions</b></span></label></span></span></div></div>
<div class="htmlSection" id="tfa_136"><div class="htmlContent" id="tfa_136-HTML"><div style="text-align: left;"><b>I agree to the processing of my personal data in accordance with</b> <b></b><b><a href="https://protect-eu.mimecast.com/s/fOQRCx2K2T198PLfAz3WC?domain=smartmoments.com.my" target="_blank">Privacy Policy</a><a target="_blank" href="https://protect-eu.mimecast.com/s/fOQRCx2K2T198PLfAz3WC?domain=smartmoments.com.my"></a></b><b>. </b></div><span style="color: rgb(116, 122, 128); font-family: Roboto, Arial, ?????, anti-alias; font-size: 15px;"><div style="text-align: center;"></div></span><span style="color: rgb(116, 122, 128); font-family: Roboto, Arial, ?????, anti-alias; font-size: 15px;"><div style="text-align: center;"></div></span></div></div>
</div>
</fieldset>
<div class="actions" id="46-A"><input type="submit" data-label="Submit" class="primaryAction" id="submit_button" value="Submit"></div>
<div style="clear:both"></div>
<input type="hidden" value="46" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="4b153b3e7e98ade80bb18ce0ebed3d49" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="31" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
</form>
</div></div><div class="wFormFooter"><p class="supportInfo"><br></p></div>
  <p class="supportInfo" >
    

      </p>
 </div>
