<!-- FORM: HEAD SECTION -->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
            let formElement = document.getElementById("tfa_0");
            if (null === formElement) {
                formElement = document.getElementById("0");
            }
            let appendJsTimerElement = function(){
                let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
                let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
                if (null !== cumulatedTimeElement) {
                    let cumulatedTime = parseInt(cumulatedTimeElement.value);
                    if (null !== cumulatedTime && cumulatedTime > 0) {
                        formTimeDiff += cumulatedTime;
                    }
                }
                let jsTimeInput = document.createElement("input");
                jsTimeInput.setAttribute("type", "hidden");
                jsTimeInput.setAttribute("value", formTimeDiff.toString());
                jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("autocomplete", "off");
                if (null !== formElement) {
                    formElement.appendChild(jsTimeInput);
                }
            };
            if (null !== formElement) {
                if(formElement.addEventListener){
                    formElement.addEventListener('submit', appendJsTimerElement, false);
                } else if(formElement.attachEvent){
                    formElement.attachEvent('onsubmit', appendJsTimerElement);
                }
            }
        });
    </script>

    <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-layout.css?v=547-4" rel="stylesheet" type="text/css" />

    <link href="https://frieslandcampina.tfaforms.net/uploads/themes/theme-2.css" rel="stylesheet" type="text/css" />
    <link href="https://frieslandcampina.tfaforms.net/dist/form-builder/5.0.0/wforms-jsonly.css?v=547-4" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/wforms.js?v=547-4"></script>
    <script type="text/javascript">
        wFORMS.behaviors.prefill.skip = false;
    </script>
        <link rel="stylesheet" type="text/css" href="https://frieslandcampina.tfaforms.net/css/kalendae.css" />
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/js/kalendae/kalendae.standalone.min.js" ></script>
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/wforms_calendar.js"></script>
    <script type="text/javascript" src="https://frieslandcampina.tfaforms.net/wForms/3.11/js/localization-ms.js?v=547-4"></script>

<!-- FORM: BODY SECTION -->
<div class="wFormContainer"  >
    <div class="wFormHeader"></div>
    <style type="text/css">
                #tfa_4,
                *[id^="tfa_4["] {
                    width: 247px !important;
                }
                #tfa_4-D,
                *[id^="tfa_4["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_140,
                *[id^="tfa_140["] {
                    width: 247px !important;
                }
                #tfa_140-D,
                *[id^="tfa_140["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_204,
                *[id^="tfa_204["] {
                    width: 247px !important;
                }
                #tfa_204-D,
                *[id^="tfa_204["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_8,
                *[id^="tfa_8["] {
                    width: 542px !important;
                }
                #tfa_8-D,
                *[id^="tfa_8["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_8-L,
                label[id^="tfa_8["] {
                    width: 392px !important;
                    min-width: 0px;
                }
            
                #tfa_184,
                *[id^="tfa_184["] {
                    width: 212px !important;
                }
                #tfa_184-D,
                *[id^="tfa_184["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_20,
                *[id^="tfa_20["] {
                    width: 321px !important;
                }
                #tfa_20-D,
                *[id^="tfa_20["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_22,
                *[id^="tfa_22["] {
                    width: 322px !important;
                }
                #tfa_22-D,
                *[id^="tfa_22["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_24,
                *[id^="tfa_24["] {
                    width: 542px !important;
                }
                #tfa_24-D,
                *[id^="tfa_24["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_24-L,
                label[id^="tfa_24["] {
                    width: 532px !important;
                    min-width: 0px;
                }
            
                #tfa_26,
                *[id^="tfa_26["] {
                    width: 542px !important;
                }
                #tfa_26-D,
                *[id^="tfa_26["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_26-L,
                label[id^="tfa_26["] {
                    width: 452px !important;
                    min-width: 0px;
                }
            
                #tfa_27,
                *[id^="tfa_27["] {
                    width: 242px !important;
                }
                #tfa_27-D,
                *[id^="tfa_27["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_27-L,
                label[id^="tfa_27["] {
                    width: 132px !important;
                    min-width: 0px;
                }
            
                #tfa_28-L,
                label[id^="tfa_28["] {
                    width: 91px !important;
                    min-width: 0px;
                }
            
                #tfa_29,
                *[id^="tfa_29["] {
                    width: 242px !important;
                }
                #tfa_29-D,
                *[id^="tfa_29["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_111,
                *[id^="tfa_111["] {
                    width: 362px !important;
                }
                #tfa_111-D,
                *[id^="tfa_111["][class~="field-container-D"] {
                    width: auto !important;
                }
            </style><div class=""><div class="wForm" id="47-WRPR" dir="ltr">
<div class="codesection" id="code-47"><style type="text/css">

#tfa_165-L {
color: red;
font-size: 110%;  
}
  
#tfa_165 {
border-bottom-width: 3px;
border-top-width: 3px;
border-right-width: 3px;
border-left-width: 3px;
}  
  
#tfa_167-L {
color: black;
}   
  
#tfa_168 {
color: black;  
 
}  
</style></div>
<form method="post" action="https://frieslandcampina.tfaforms.net/responses/processor" class="hintsBelow labelsAbove" id="47" role="form">
<fieldset id="tfa_146" class="section wf-acl-hidden">
<legend id="tfa_146-L">Hidden Fields</legend>
<div id="tfa_154" class="section inline group">
<div class="oneField field-container-D    " id="tfa_147-D">
<label id="tfa_147-L" class="label preField " for="tfa_147">Last Campaign</label><br><div class="inputWrapper"><input type="text" id="tfa_147" name="tfa_147" value="Dutch Lady Mom's Club" default="Dutch Lady Mom's Club" title="Last Campaign" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="oneField field-container-D    " id="tfa_148-D">
<label id="tfa_148-L" class="label preField " for="tfa_148">Lead Source</label><br><div class="inputWrapper"><input type="text" id="tfa_148" name="tfa_148" value="Website" default="Website" title="Lead Source" data-dataset-allow-free-responses="" class=""></div>
</div>
</div>
<div id="tfa_155" class="section inline group">
<div class="oneField field-container-D    " id="tfa_149-D">
<label id="tfa_149-L" class="label preField " for="tfa_149">UTM_Source</label><br><div class="inputWrapper"><input type="text" id="tfa_149" name="tfa_149" value="" title="UTM_Source" data-dataset-allow-free-responses="" class="calc-UTMsource"></div>
</div>
<div class="oneField field-container-D    " id="tfa_150-D">
<label id="tfa_150-L" class="label preField " for="tfa_150">UTM_Medium</label><br><div class="inputWrapper"><input type="text" id="tfa_150" name="tfa_150" value="" title="UTM_Medium" data-dataset-allow-free-responses="" class="calc-UTMmedium"></div>
</div>
</div>
<div id="tfa_156" class="section inline group">
<div class="oneField field-container-D    " id="tfa_151-D">
<label id="tfa_151-L" class="label preField " for="tfa_151">UTM_Campaign</label><br><div class="inputWrapper"><input type="text" id="tfa_151" name="tfa_151" value="" title="UTM_Campaign" data-dataset-allow-free-responses="" class="calc-UTMcampaign"></div>
</div>
<div class="oneField field-container-D    " id="tfa_153-D">
<label id="tfa_153-L" class="label preField " for="tfa_153">UTM_Content</label><br><div class="inputWrapper"><input type="text" id="tfa_153" name="tfa_153" value="" title="UTM_Content" data-dataset-allow-free-responses="" class="calc-UTMcontent"></div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_212-D">
<label id="tfa_212-L" class="label preField " for="tfa_212">UTM (Concatenated)</label><br><div class="inputWrapper"><input type="text" id="tfa_212" name="tfa_212" value="" readonly title="UTM (Concatenated)" data-dataset-allow-free-responses="" class='formula=utmsource+"/"+utmmedium+"/"+utmcampaign+"/"+utmcontent readonly'></div>
</div>
<div class="oneField field-container-D    " id="tfa_163-D" role="group" aria-labelledby="tfa_163-L">
<label id="tfa_163-L" class="label preField "><b>ACTIVITY TYPE</b></label><br><div class="inputWrapper"><span id="tfa_163" class="choices vertical "><span class="oneChoice"><input type="checkbox" value="tfa_164" class="" checked data-default-value="true" id="tfa_164" name="tfa_164" aria-labelledby="tfa_164-L"><label class="label postField" id="tfa_164-L" for="tfa_164"><span class="input-checkbox-faux"></span>Sample Request</label></span></span></div>
</div>
</fieldset>
<div class="oneField field-container-D    " id="tfa_4-D">
<label id="tfa_4-L" class="label preField reqMark" for="tfa_4"><b>SILA PILIH PRODUK SAMPEL ANDA</b><b></b></label><br><div class="inputWrapper"><select id="tfa_4" name="tfa_4" title="SILA PILIH PRODUK SAMPEL ANDA" aria-required="true" class="calc-product required"><option value="">Sila pilih ...</option>
<option value="tfa_5" id="tfa_5" data-conditionals="#tfa_52,#tfa_140" class="calcval-DutchLady123">Dutch Lady 123</option>
<option value="tfa_6" id="tfa_6" data-conditionals="#tfa_52,#tfa_204" class="">Dutch Lady 456</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_140-D">
<label id="tfa_140-L" class="label preField reqMark" for="tfa_140"><b>SILA PILIH PERISA</b></label><br><div class="inputWrapper"><select id="tfa_140" name="tfa_140" data-condition="`#tfa_5`" title="SILA PILIH PERISA" aria-required="true" class="calc-dlflavora required"><option value="">Sila pilih ...</option>
<option value="tfa_141" id="tfa_141" class="calcval-01t6F00000B6LlTQAV">Asli</option>
<option value="tfa_142" id="tfa_142" class="calcval-01t6F00000B6LlUQAV">Madu</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_204-D">
<label id="tfa_204-L" class="label preField reqMark" for="tfa_204"><b>SILA PILIH PERISA</b></label><br><div class="inputWrapper"><select id="tfa_204" name="tfa_204" data-condition="`#tfa_6`" title="SILA PILIH PERISA" aria-required="true" class="calc-dlflavorb required"><option value="">Sila pilih ...</option>
<option value="tfa_209" id="tfa_209" class="calcval-01t6F00000B6LlVQAV">Asli</option>
<option value="tfa_210" id="tfa_210" class="calcval-01t6F00000B6LlWQAV">Coklat</option></select></div>
</div>
<fieldset id="tfa_169" class="section wf-acl-hidden">
<legend id="tfa_169-L">Product Selected</legend>
<div class="oneField field-container-D    " id="tfa_211-D">
<label id="tfa_211-L" class="label preField " for="tfa_211">Product Chosen (ID)</label><br><div class="inputWrapper"><input type="text" id="tfa_211" name="tfa_211" value="" readonly title="Product Chosen (ID)" data-dataset-allow-free-responses="" class='formula=if(product=="DutchLady123"){dlflavora}else{dlflavorb} readonly'></div>
</div>
</fieldset>
<fieldset id="tfa_7" class="section">
<legend id="tfa_7-L">Bahagian 1: Butiran Diri</legend>
<div class="oneField field-container-D    " id="tfa_8-D">
<label id="tfa_8-L" class="label preField reqMark" for="tfa_8"><b>NAMA (SEPERTI DI DALAM MYKAD/PASPORT)</b></label><br><div class="inputWrapper"><input type="text" id="tfa_8" name="tfa_8" value="" aria-required="true" title="NAMA (SEPERTI DI DALAM MYKAD/PASPORT)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div id="tfa_195" class="section inline group">
<div class="oneField field-container-D    " id="tfa_184-D">
<label id="tfa_184-L" class="label preField reqMark" for="tfa_184"><b>NOMBOR TELEFON BIMBIT</b></label><br><div class="inputWrapper"><select id="tfa_184" name="tfa_184" title="NOMBOR TELEFON BIMBIT" aria-required="true" class="required"><option value="">Sila pilih ...</option>
<option value="tfa_185" id="tfa_185" class="">010</option>
<option value="tfa_186" id="tfa_186" class="">011</option>
<option value="tfa_187" id="tfa_187" class="">012</option>
<option value="tfa_188" id="tfa_188" class="">013</option>
<option value="tfa_189" id="tfa_189" class="">014</option>
<option value="tfa_190" id="tfa_190" class="">015</option>
<option value="tfa_191" id="tfa_191" class="">016</option>
<option value="tfa_192" id="tfa_192" class="">017</option>
<option value="tfa_193" id="tfa_193" class="">018</option>
<option value="tfa_194" id="tfa_194" class="">019</option></select></div>
</div>
<div class="oneField field-container-D  labelsHidden  " id="tfa_20-D">
<label id="tfa_20-L" class="label preField reqMark" for="tfa_20">NUMBER</label><br><div class="inputWrapper"><input type="text" id="tfa_20" name="tfa_20" value="" aria-required="true" autoformat="########" title="NUMBER" data-dataset-allow-free-responses="" class="validate-custom /^([0-9]{7,8})$/ required"></div>
<script type="text/javascript">
                        if(typeof wFORMS != 'undefined') {
                            if(wFORMS.behaviors.validation) {
                                wFORMS.behaviors.validation.rules['customtfa_20'] =  { selector: '*[id="tfa_20"]', check: 'validateCustom'};
                                wFORMS.behaviors.validation.messages['customtfa_20'] = "Sila masukkan 7 atau 8-digit nombor telefon bimbit anda.";
                            }
                        }</script>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_22-D">
<label id="tfa_22-L" class="label preField reqMark" for="tfa_22"><b>EMEL</b></label><br><div class="inputWrapper"><input type="text" id="tfa_22" name="tfa_22" value="" aria-required="true" title="EMEL" data-dataset-allow-free-responses="" class="validate-email required"></div>
</div>
</fieldset>
<fieldset id="tfa_23" class="section">
<legend id="tfa_23-L">Bahagian 2: Ke manakah harus kami membuat penghantaran sampel?</legend>
<div class="oneField field-container-D    " id="tfa_24-D">
<label id="tfa_24-L" class="label preField reqMark" for="tfa_24"><b>ALAMAT BARISAN 1 </b><i><b>(No. Rumah/Unit, Nama Kondo/Apartmen)</b></i></label><br><div class="inputWrapper"><input type="text" id="tfa_24" name="tfa_24" value="" aria-required="true" title="ALAMAT BARISAN 1 (No. Rumah/Unit, Nama Kondo/Apartmen)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_26-D">
<label id="tfa_26-L" class="label preField reqMark" for="tfa_26"><b>ALAMAT BARISAN 2&nbsp;<i>(N</i><i>ama Jalan/Daerah)</i></b></label><br><div class="inputWrapper"><input type="text" id="tfa_26" name="tfa_26" value="" aria-required="true" title="ALAMAT BARISAN 2 (Nama Jalan/Daerah)" data-dataset-allow-free-responses="" class="required"></div>
</div>
<div id="tfa_33" class="section inline group">
<div class="oneField field-container-D    " id="tfa_27-D">
<label id="tfa_27-L" class="label preField reqMark" for="tfa_27"><b>POSKOD</b><b></b></label><br><div class="inputWrapper"><input type="text" id="tfa_27" name="tfa_27" value="" aria-required="true" autoformat="#####" title="POSKOD" data-dataset-allow-free-responses="" class="validate-custom /^([0-9]{5})$/ required"></div>
<script type="text/javascript">
                        if(typeof wFORMS != 'undefined') {
                            if(wFORMS.behaviors.validation) {
                                wFORMS.behaviors.validation.rules['customtfa_27'] =  { selector: '*[id="tfa_27"]', check: 'validateCustom'};
                                wFORMS.behaviors.validation.messages['customtfa_27'] = "Please enter your 5-digit post code.";
                            }
                        }</script>
</div>
<div class="oneField field-container-D    " id="tfa_28-D">
<label id="tfa_28-L" class="label preField reqMark" for="tfa_28"><b></b><b>BANDAR</b><b></b></label><br><div class="inputWrapper"><input type="text" id="tfa_28" name="tfa_28" value="" aria-required="true" title="BANDAR" data-dataset-allow-free-responses="" class="required"></div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_29-D">
<label id="tfa_29-L" class="label preField reqMark" for="tfa_29"><b>NEGERI</b><b></b></label><br><div class="inputWrapper"><select id="tfa_29" name="tfa_29" title="NEGERI" aria-required="true" class="required"><option value="">Sila pilih ...</option>
<option value="tfa_30" id="tfa_30" class="">Johor Darul Takzim</option>
<option value="tfa_31" id="tfa_31" class="">Kedah Darul Aman</option>
<option value="tfa_32" id="tfa_32" class="">Kelantan Darul Naim</option>
<option value="tfa_34" id="tfa_34" class="">Melaka</option>
<option value="tfa_35" id="tfa_35" class="">Negeri Sembilan Darul Khusus</option>
<option value="tfa_36" id="tfa_36" class="">Pahang Darul Makmur</option>
<option value="tfa_37" id="tfa_37" class="">Perak Darul Ridzuan</option>
<option value="tfa_38" id="tfa_38" class="">Perlis Indera Kayangan</option>
<option value="tfa_39" id="tfa_39" class="">Pulau Pinang</option>
<option value="tfa_40" id="tfa_40" class="">Sabah</option>
<option value="tfa_41" id="tfa_41" class="">Sarawak</option>
<option value="tfa_42" id="tfa_42" class="">Selangor Darul Ehsan</option>
<option value="tfa_43" id="tfa_43" class="">Terengganu Darul Iman</option>
<option value="tfa_44" id="tfa_44" class="">Wilayah Persekutuan Kuala Lumpur</option>
<option value="tfa_45" id="tfa_45" class="">Wilayah Persekutuan Labuan</option>
<option value="tfa_46" id="tfa_46" class="">Wilayah Persekutuan Putrajaya</option></select></div>
</div>
</fieldset>
<fieldset id="tfa_52" class="section" data-condition="`#tfa_5` OR `#tfa_6`">
<legend id="tfa_52-L">Bahagian 3: Ceritakan sedikit tentang Si Manja anda</legend>
<div class="oneField field-container-D    " id="tfa_161-D">
<label id="tfa_161-L" class="label preField reqMark" for="tfa_161"><span style="font-weight: 700; font-size: 14.4px;">TARIKH LAHIR</span></label><br><div class="inputWrapper"><input type="text" id="tfa_161" name="tfa_161" value="" autocomplete="off" aria-required="true" min="2012-01-01" title="TARIKH LAHIR" data-dataset-allow-free-responses="" class="validate-datecal required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_111-D">
<label id="tfa_111-L" class="label preField " for="tfa_111"><b>JENAMA SUSU YANG DIMINUM</b><br></label><br><div class="inputWrapper"><select id="tfa_111" name="tfa_111" title="JENAMA SUSU YANG DIMINUM" class=""><option value="">Sila pilih ...</option>
<option value="tfa_112" id="tfa_112" class="">Abbott</option>
<option value="tfa_113" id="tfa_113" class="">Anmum Essential</option>
<option value="tfa_143" id="tfa_143" class="">Bebelac</option>
<option value="tfa_144" id="tfa_144" class="">Bonlife</option>
<option value="tfa_114" id="tfa_114" class="">Dumex</option>
<option value="tfa_115" id="tfa_115" class="">Dumex Mamil</option>
<option value="tfa_116" id="tfa_116" class="">Dutch Lady</option>
<option value="tfa_117" id="tfa_117" class="">Enfagrow</option>
<option value="tfa_118" id="tfa_118" class="">Fernleaf</option>
<option value="tfa_119" id="tfa_119" class="">Friso Gold</option>
<option value="tfa_120" id="tfa_120" class="">Gain Kid</option>
<option value="tfa_145" id="tfa_145" class="">Gain Plus</option>
<option value="tfa_121" id="tfa_121" class="">Isomil</option>
<option value="tfa_122" id="tfa_122" class="">Lactogen</option>
<option value="tfa_123" id="tfa_123" class="">Morinaga</option>
<option value="tfa_124" id="tfa_124" class="">Nan</option>
<option value="tfa_125" id="tfa_125" class="">Novalac</option>
<option value="tfa_126" id="tfa_126" class="">Pediasure</option>
<option value="tfa_127" id="tfa_127" class="">S-26</option>
<option value="tfa_128" id="tfa_128" class="">Snow</option>
<option value="tfa_129" id="tfa_129" class="">Sustagen</option>
<option value="tfa_130" id="tfa_130" class="">Tidak mengambil susu tepung</option>
<option value="tfa_131" id="tfa_131" class="">Lain-lain</option></select></div>
</div>
</fieldset>
<fieldset id="tfa_165" class="section">
<legend id="tfa_165-L">Penyerahan<br><div></div><div></div></legend>
<div class="oneField field-container-D  labelsRemoved  " id="tfa_166-D" role="group" aria-labelledby="tfa_166-L"><div class="inputWrapper"><span id="tfa_166" class="choices vertical required"><span class="oneChoice"><input type="checkbox" value="tfa_167" class="" id="tfa_167" name="tfa_167" aria-labelledby="tfa_167-L"><label class="label postField" id="tfa_167-L" for="tfa_167"><span class="input-checkbox-faux"></span><b>Setuju dengan Terma dan Syarat</b></label></span></span></div></div>
<div class="htmlSection" id="tfa_168"><div class="htmlContent" id="tfa_168-HTML"><b>Saya mengizinkan pemprosesan maklumat peribadi saya mengikut <a target="_blank" href="https://protect-eu.mimecast.com/s/j_VeCwKJKHGA6gviQS2XM?domain=smartmoments.com.my">Polisi Privasi</a>.</b><br></div></div>
</fieldset>
<div class="actions" id="47-A"><input type="submit" data-label="Hantar" class="primaryAction" id="submit_button" value="Hantar"></div>
<div style="clear:both"></div>
<input type="hidden" value="47" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="98a3f69bcea4db627d09c1509b62d995" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="25" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
</form>
</div></div><div class="wFormFooter"><p class="supportInfo"><br></p></div>
  <p class="supportInfo" >
    

      </p>
 </div>
