<?php
/*
Template Name: Under Construction Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $sorry = 'This is page is under construction.';

      if (function_exists('pll__')) {
				$sorry = pll__('This is page is under construction.');
      }
    ?>
      <div class="main-container">
        <div class="under-construction">
          <h1 class="under-construction-title"><?php echo esc_attr($sorry); ?></h1>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

