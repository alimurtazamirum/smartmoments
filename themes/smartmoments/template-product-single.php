<?php
/*
Template Name: Product Single Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $products = 'Products';
      $buyNow = 'Buy Now';
      $productInfo = 'Product Information';
      $nutriInfo = 'Nutritional Information';
      $keyNutrients = 'Key Nutrients';
      $flavorsAndPacks = 'Flavours and Pack Sizes';
      $preparationMethod = 'Preparation Method';
      $storageMethod = 'Storage Method';
      $avgComp = 'Average Composition';
      $per100g = 'Per 100g';
      $perServing = 'Per serving 220ml';
      $aboutOther = 'Learn about other Dutch with 5x DHA products';

      if (function_exists('pll__')) {
        $products = pll__('Products');
        $buyNow = pll__('Buy Now');
        $productInfo = pll__('Product Information');
        $nutriInfo = pll__('Nutritional Information');
        $keyNutrients = pll__('Key Nutrients');
        $flavorsAndPacks = pll__('Flavours and Pack Sizes');
        $preparationMethod = pll__('Preparation Method');
        $storageMethod = pll__('Storage Method');
        $avgComp = pll__('Average Composition');
        $per100g = pll__('Per 100g');
        $perServing = pll__('Per serving 220ml');
        $aboutOther = pll__('About Other Products');
      }
    ?>
      <?php
        $headerImg = get_field('header_image');
        $headerImgMob = get_field('header_image_mobile');
        if( !empty($headerImg) && !empty($headerImgMob) ):
      ?>
      <div class="sm-header-responsive" style="background-image: url('<?php echo $headerImg['url']; ?>');" >
      </div>
      <div class="sm-header-responsive mobile" style="background-image: url('<?php echo $headerImgMob['url']; ?>');" >
      </div>
      <?php endif; ?>
      <div class="main-container">
        <section class="product-desc">
          <h2 class="product-desc-title"><?php the_title(); ?></h2>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; ?>
          <a class="sm-btn-link lg" href="<?php the_field('buy_now_url', 'option'); ?>"><?php echo esc_attr($buyNow); ?></a>
        </section>
        <section class="product-info">
          <div class="product-info-tabs">
            <button id="product-info-content" class="sm-btn-tab active"><?php echo esc_attr($productInfo); ?></button>
            <button id="product-info-nutrition" class="sm-btn-tab"><?php echo esc_attr($nutriInfo); ?></button>
          </div>
          <div class="product-info-content">
            <div class="product-content-box key">
              <?php $productImg = get_field('product_single_image'); ?>
              <img class="product-content-image" src="<?php echo $productImg['url']; ?>" alt="<?php echo $productImg['alt']; ?>">
              <div class="product-content">
                <h3 class="product-content-heading"><?php echo esc_attr($keyNutrients); ?></h3>
                <?php the_field('key_nutrients'); ?>
              </div>         
            </div>
            <div class="product-content-box">
              <h3 class="product-content-heading"><?php echo esc_attr($flavorsAndPacks); ?></h3>
              <?php the_field('flavours_and_pack_sizes'); ?>
            </div>
            <div class="product-content-box">
              <h3 class="product-content-heading"><?php echo esc_attr($preparationMethod); ?></h3>
              <?php the_field('preparation_method'); ?>
              <h3 class="product-content-heading"><?php echo esc_attr($storageMethod); ?></h3>
              <?php the_field('storage_method'); ?>
            </div>
          </div>
          <?php
            $flavors = get_field('product_flavor');
            $tdInfo = get_field('nutritional_info');
          ?>
          <div class="product-info-nutrition">
            <table class="tbl-nutrition">
              <?php if($flavors): ?>
              <tr>
                <th></th>
                <?php foreach($flavors as $flavor): ?>
                <?php $flavorImg = $flavor['product_flavor_image']; ?>
                <th colspan="2" class="tbl-product-img">
                  <img src="<?php echo $flavorImg['url']; ?>" alt="<?php echo $flavorImg['alt']; ?>">
                </th>
                <?php endforeach; ?>
              </tr>
              <tr>
                <th></th>
                <?php foreach($flavors as $flavor): ?>
                <?php $flavorName = $flavor['product_flavor_name']; ?>
                <th colspan="2" class="tbl-header">
                  <?php echo esc_attr($flavorName); ?>
                </th>
                <?php endforeach; ?>
              </tr>
              <?php endif; ?>
              <tr>
                <td></td>
                <td colspan="6" class="tbl-source"><?php the_field('source'); ?></td>
              </tr>
              <tr>
                <th class="tbl-header"><?php echo esc_attr($avgComp); ?></th>
                <?php if($flavors[0]): ?>
                <th class="tbl-header"><?php echo esc_attr($per100g); ?></th>
                <th class="tbl-header"><?php echo esc_attr($perServing); ?></th>
                <?php endif; ?>
                <?php if($flavors[1]): ?>
                <th class="tbl-header"><?php echo esc_attr($per100g); ?></th>
                <th class="tbl-header"><?php echo esc_attr($perServing); ?></th>
                <?php endif; ?>
                <?php if($flavors[2]): ?>
                <th class="tbl-header"><?php echo esc_attr($per100g); ?></th>
                <th class="tbl-header"><?php echo esc_attr($perServing); ?></th>
                <?php endif; ?>
              </tr>

              <?php foreach($tdInfo as $info): ?>
                <?php
                  $tdClass = 'tbl-td';
                  if($info['emphasize'] === true) {
                    $tdClass = 'tbl-td main';
                  }
                ?>
                <tr>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['average_composition']); ?>
                  </td>
                  <?php if($flavors[0]): ?>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_100g_product_1']); ?>
                  </td>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_serving_product_1']); ?>
                  </td>
                  <?php endif; ?>
                  <?php if($flavors[1]): ?>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_100g_product_2']); ?>
                  </td>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_serving_product_2']); ?>
                  </td>
                  <?php endif; ?>
                  <?php if($flavors[2]): ?>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_100g_product_3']); ?>
                  </td>
                  <td class="<?php echo esc_attr($tdClass); ?>">
                    <?php echo esc_attr($info['per_serving_product_3']); ?>
                  </td>
                </tr>
                <?php endif; ?>
              <?php endforeach; ?>
            </table>
          </div>
        </section>
        <?php
          $productLinks = get_field('other_product_links');
        ?>
        <?php if( $productLinks ) : ?>
        <section class="product-others">
          <h2 class="product-others-title"><?php echo esc_attr($aboutOther); ?></h2>
          <div class="product-others-links">
            <?php
              $homeUrl = get_site_url() . '/';
              if (function_exists('pll__')) {
                $homeUrl = pll_home_url();
              }
            ?>
            <?php if( in_array('Dutch Lady 123', $productLinks) ) : ?>
              <a class="sm-btn-link dl123" href="<?php echo $homeUrl; ?>products/dutch-lady-123">Dutch Lady® 123</a>
            <?php endif; ?>
            <?php if( in_array('Dutch Lady 456', $productLinks) ) : ?>
              <a class="sm-btn-link dl456" href="<?php echo $homeUrl; ?>products/dutch-lady-456">Dutch Lady® 456</a>
            <?php endif; ?>
            <?php if( in_array('Dutch Lady 6+', $productLinks) ) : ?>
              <a class="sm-btn-link dl6" href="<?php echo $homeUrl; ?>products/dutch-lady-6">Dutch Lady® 6+</a>
            <?php endif; ?>
          </div>
        </section>
        <?php endif; ?>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

