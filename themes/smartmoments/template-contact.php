<?php
/*
Template Name: Contact Us Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $featuredImageUrl = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
      <div class="sm-header" style="background-image: url('<?php echo esc_url($featuredImageUrl); ?>');">
        <div class="sm-header-overlay">
          <div class="sm-header-content">
            <h1 class="sm-header-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
      <div class="main-container">
        <div class="contact-container">
          <div class="contact-item-container">
            <div class="contact-item">
              <img class="contact-item-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon_phone.svg" alt="Phone Icon">
              <div class="contact-item-content">
                <h3 class="contact-item-title"><?php the_field('careline'); ?><br/><?php the_field('careline_number'); ?></h3>
                <div class="contact-item-desc">
                  <?php the_field('careline_description'); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="contact-item-container">
            <div class="contact-item">
              <img class="contact-item-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon_post.svg" alt="Post Icon">
              <div class="contact-item-content">
                <h3 class="contact-item-title"><?php the_field('post'); ?></h3>
                <div class="contact-item-desc">
                  <?php the_field('post_address'); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="contact-item-container">
            <div class="contact-item">
              <img class="contact-item-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon_mail.svg" alt="Mail Icon">
              <div class="contact-item-content">
                <h3 class="contact-item-title"><?php the_field('email_title'); ?></h3>
                <div class="contact-item-desc">
                  <a class="sm-link" href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="contact-item-container">
            <div class="contact-item">
              <img class="contact-item-icon" src="<?php bloginfo( 'template_url' ); ?>/assets/img/icon_facebook.svg" alt="Facebook Icon">
              <div class="contact-item-content">
                <h3 class="contact-item-title"><?php the_field('social_title'); ?></h3>
                <div class="contact-item-desc">
                  <a class="sm-link" href="<?php the_field('facebook_link'); ?>"><?php the_field('facebook_link'); ?></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

