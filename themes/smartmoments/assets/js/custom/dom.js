jQuery(document).ready(function($) {
  var $window = $(window);

  /** Important Notice */
  var $importantOverlay = $('#important-overlay');
  var $acceptBtn = $('#important-accept-btn');
  if(localStorage.getItem('smAgree') !== 'accept') {
    $importantOverlay.show();
  }
  $acceptBtn.on('click', function(e) {
    e.preventDefault();
    localStorage.setItem('smAgree', 'accept');
    $importantOverlay.fadeOut(200);
  });


  /** Home **/
  var $homeCarousel = $('.home-carousel-container');
  $homeCarousel.slick({
    dots: true,
    arrows: true,
    autoplay: true
  });

  var $videoOverlay = $('.home-overlay.for-video');
  var $video = $('.home-video-iframe');
  var $videoBox = $('.home-content-box.vid > a');
  var id;
  $videoBox.on('click', function(e) {
    e.preventDefault();
    id = $(this).attr('id');
    $videoOverlay.fadeIn(200);
    $('#video-' + id).fadeIn(200);
  });

  $videoOverlay.on('click', function(e) {
    var $iframe = $('#video-' + id).find('iframe');
    var leg = $iframe.attr('src');
    $iframe.attr('src', leg); //This stops video playback when we hide
    $videoOverlay.fadeOut(200);
    $video.fadeOut(200);
  });
  
  /** Header **/
  var productDropdown = '<button class="nav-products-dd"></button>';
  var $productsLink = $('.nav-link-products');
  $productsLink.append(productDropdown);
  var $productsDropdown = $('.nav-products-dd');
  var $productsSubLinks = $('.nav-link-products .sub-menu');

  $productsDropdown.on('click', function(e) {
    e.preventDefault();
    $productsSubLinks.toggle(200);
  });

  var $headerMobileBtn = $('.header-mobile-btn');
  var $headerControls = $('.header-controls');
  $headerMobileBtn.on('click', function(e) {
    e.preventDefault();
    $headerControls.toggleClass('show');
  });

  // Remove inline styles on window resize
  $window.on('resize', function() {
    $productsSubLinks.removeAttr('style');
  });

  /** Product Single Page Tabs **/
  var $productTab = $('.product-info-tabs > .sm-btn-tab');
  var $productInfoContent = $('.product-info-content');
  var $productInfoNutri = $('.product-info-nutrition');

  $productTab.on('click', function(e) {
    var targetStr = e.target.id;
    var $activeBtn = $('#' + targetStr);
    if (! $activeBtn.hasClass('active')) {
      $productTab.removeClass('active');
      $activeBtn.addClass('active');
      $productInfoContent.toggle();
      $productInfoNutri.toggle();
    }
  });

  /** FAQ  **/
  var $faqQuestion = $('.faq-question-container');
  $faqQuestion.on('click', function() {
    $(this).next('.faq-answer').slideToggle(200);
    $(this).toggleClass('active');
  });

  /** Form Icons **/
  var $formOverlay = $('#form-overlay');
  var $formIcon = $('.form-icon');
  var $formIconModal = $('.form-icon-modal');
  var iconId;

  $formOverlay.on('click', function() {
    $formOverlay.fadeOut(200);
    $formIconModal.fadeOut(200);
  });
  
  $formIcon.on('click', function(e) {
    console.log(e.target);
    iconId = e.target.id;
    $formOverlay.fadeIn(200);
    $('#modal-' + iconId).fadeIn(200);
  });

});

