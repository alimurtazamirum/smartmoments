<?php
/*
Template Name: FAQ Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <div class="main-container">
        <h1 class="faq-title"><?php the_title(); ?></h1>
        <div class="faq-container">
          <?php
            $faqs = get_field('faqs');
          ?>
          <?php if($faqs): ?>
          <?php foreach($faqs as $faq): ?>
          <div class="faq-item">
            <div class="faq-question-container">
              <div class="faq-icon">
                <span></span>
                <span></span>
              </div>
              <h3 class="faq-question"><?php echo $faq['question']; ?></h3>
            </div>
            <div class="faq-answer">
              <?php echo $faq['answer']; ?>
            </div>
          </div>
          <?php endforeach; ?>
          <?php endif; ?>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

