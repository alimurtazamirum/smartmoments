<?php
/*
Template Name: Articles Single Category Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
        $articles = get_field('articles');
        $videos = get_field('videos');
        $headerImg = get_field('header_image');
        $headerImgMob = get_field('header_image_mobile');
        if( !empty($headerImg) && !empty($headerImgMob) ):
      ?>
      <div class="sm-header-responsive" style="background-image: url('<?php echo $headerImg['url']; ?>');" >
      </div>
      <div class="sm-header-responsive mobile" style="background-image: url('<?php echo $headerImgMob['url']; ?>');" >
      </div>
      <?php endif; ?>
      <div class="main-container">
        <section class="article-posts-container">
          <?php if($articles): ?>
          <?php foreach($articles as $article): ?>
          <article class="article-container">
            <h2 class="article-heading"><?php echo esc_attr($article['article_category']); ?></h2>
            <h1 class="article-heading main"><?php echo esc_attr($article['article_heading']); ?></h1>
            <p class="article-subheading"><?php echo esc_attr($article['article_subheading']); ?></p>
            <div class="article-content">
              <?php echo $article['article_content']; ?>
            </div>
          </article>
          <?php endforeach; ?>
          <?php endif; ?>

          <?php if($videos): ?>
          <?php foreach($videos as $video): ?>
          <article class="article-container">
            <h2 class="article-heading"><?php echo esc_attr($video['video_category']); ?></h2>
            <h1 class="article-heading main"><?php echo esc_attr($video['video_title']); ?></h1>
            <p class="article-subheading"><?php echo esc_attr($video['video_description']); ?></p>
            <div class="article-video-iframe">
              <div class="iframe-container">
                <iframe
                  src="https://www.youtube.com/embed/<?php echo esc_attr($video['video_id']); ?>"
                  frameborder="0"
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                >
                </iframe>
              </div> 
            </div>
          </article>
          <?php endforeach; ?>
          <?php endif; ?>
        </section>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

