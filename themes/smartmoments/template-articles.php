<?php
/*
Template Name: Articles Main Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php 
        $featuredImageUrl = get_the_post_thumbnail_url(get_the_ID(), 'full');
      ?>
      <div class="articles-header" style="background-image: url('<?php echo esc_url($featuredImageUrl); ?>');">
        <!-- <div class="articles-header-overlay">
          <div class="articles-header-content">
            <h1 class="articles-header-title"><?php // the_title(); ?></h1>
            <p><?php // the_field('articles_header_description'); ?></p>
          </div>    
        </div> -->
      </div>
      <div class="main-container">
        <?php
          $args = array(
            'posts_per_page'   => -1,
            'post_type'        => 'page',
            'order'            => 'ASC',
            'orderby'          => 'title',
            'post_parent'      => 118,
          );
          $the_query = new WP_Query($args);
        ?>

        <?php if  ($the_query->have_posts() ) : ?>
        <section class="articles-cat-container">
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <a class="articles-cat-item" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('full'); ?>
            <div class="articles-cat-item-desc">
              <h2 class="articles-cat-item-title"><?php the_title(); ?></h2>
              <p><?php the_field('category_description'); ?></p>
            </div>
          </a>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        </section>
        <?php endif; ?>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

