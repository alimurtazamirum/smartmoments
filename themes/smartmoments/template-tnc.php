<?php
/*
Template Name: T&C Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $featuredImageUrl = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
      <div class="sm-header" style="background-image: url('<?php echo esc_url($featuredImageUrl); ?>');">
        <div class="sm-header-overlay">
          <div class="sm-header-content">
            <h1 class="sm-header-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
      <div class="main-container">
        <div class="sm-content-container">
        <?php while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; ?>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

