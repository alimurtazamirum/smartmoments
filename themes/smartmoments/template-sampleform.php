<?php
/*
Template Name: Sample Form Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
    <?php
      $featuredImageUrl = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
      <?php
        $headerImg = get_field('header_image');
        $headerImgMob = get_field('header_image_mobile');
        if( !empty($headerImg) && !empty($headerImgMob) ):
      ?>
      <div class="sm-header-responsive" style="background-image: url('<?php echo $headerImg['url']; ?>');" >
      </div>
      <div class="sm-header-responsive mobile" style="background-image: url('<?php echo $headerImgMob['url']; ?>');" >
      </div>
      <?php endif; ?>
      <div class="main-container">
         <?php
            if (function_exists('pll__')) {
              $formTemplate = 'template-parts/sample-form-' . pll_current_language();
              $formIcons = 'template-parts/sample-form-icons-' . pll_current_language();
            } else {
              $formTemplate = 'template-parts/sample-form-en';
              $formIcons = 'template-parts/sample-form-icons-en';
            }
          ?>
          <?php get_template_part($formIcons); ?>
        <div class="sample-form-container">
          <?php get_template_part($formTemplate); ?>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

