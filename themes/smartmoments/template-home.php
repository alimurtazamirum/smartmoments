<?php
/*
Template Name: Home Page
Template Post Type: page
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
        $slides = get_field('carousel');
        $boxes = get_field('content_boxes');
      ?>
      <div class="home-overlay for-video">
        <?php if($boxes): ?>
        <?php foreach($boxes as $key=>$boxItem): ?>
        <div id="video-<?php echo $key; ?>" class="home-video-iframe">
          <div class="iframe-container">
            <iframe
              id="iframe-<?php echo $key; ?>"
              src="https://www.youtube.com/embed/<?php echo esc_attr($boxItem['video_id']); ?>"
              frameborder="0"
              allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            >
            </iframe>
          </div> 
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <?php if($slides): ?>
      <div class="home-carousel-container">
        <?php foreach($slides as $slide): 
          $slideImg = $slide['carousel_image_desktop'];
        ?>
        <div class="home-carousel-item">
          <img src="<?php echo $slideImg['url']; ?>" alt="<?php echo $slideImg['alt']; ?>">
        </div>
        <?php endforeach; ?>
      </div>
      <div class="home-carousel-container mobile">
        <?php foreach($slides as $slide): 
          $mobSlideImg = $slide['carousel_image_mobile'];
        ?>
        <div class="home-carousel-item">
          <img src="<?php echo $mobSlideImg['url']; ?>" alt="<?php echo $mobSlideImg['alt']; ?>">
        </div>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>
      <div class="main-container">
        <?php if($boxes): ?>
        <div class="home-content-box-container">
          <?php foreach($boxes as $key=>$boxItem): 
            $ctaImg = $boxItem['cta_image'];
            $videoImg = $boxItem['video_image'];
            $ctaClass = 'home-cta-content';
            if($boxItem['blue_text'] === true) {
              $ctaClass = 'home-cta-content alt';
            }
          ?>
          <div class="home-content-box cta">
            <div class="<?php echo esc_attr($ctaClass); ?>">
              <h3 class="home-cta-content-title"><?php echo esc_attr($boxItem['cta_title']); ?></h3>
              <?php if($boxItem['cta_description']) : ?>
              <p class="home-cta-content-desc"><?php echo esc_attr($boxItem['cta_description']); ?></p>
              <?php endif; ?>
              <a class="sm-btn-link" href="<?php echo esc_url($boxItem['cta_link']); ?>"><?php echo esc_attr($boxItem['cta_button_text']); ?></a>
            </div>
            <img src="<?php echo $ctaImg['url']; ?>" alt="<?php echo $ctaImg['alt']; ?>">
          </div>
          <div class="home-content-box vid">
            <a id="<?php echo $key ?>" href="https://www.youtube.com/watch?v=<?php echo esc_attr($boxItem['video_id']); ?>" target="_blank">
              <img src="<?php echo $videoImg['url']; ?>" alt="<?php echo $videoImg['alt']; ?>">
            </a>
          </div>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

